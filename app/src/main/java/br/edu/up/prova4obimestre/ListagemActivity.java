package br.edu.up.prova4obimestre;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListagemActivity extends AppCompatActivity {

    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        Cursor resultado = db.rawQuery("select * from produtos",null);
        List<String> produtos = new ArrayList<>();
        while(resultado.moveToNext()){
            String nome = resultado.getString(resultado.getColumnIndex("nome"));
            String valor = resultado.getString(resultado.getColumnIndex("valor"));
            produtos.add(nome + ", " + valor);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, produtos);

        ListView listView = (ListView) findViewById(R.id.lstProdutos);
        listView.setAdapter(adapter);

    }
}
