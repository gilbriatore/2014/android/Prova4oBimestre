package br.edu.up.prova4obimestre;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        //db.execSQL("DROP TABLE produtos");

        db.execSQL("CREATE TABLE IF NOT EXISTS produtos (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, valor REAL)");

    }

    public void incluir(View view) {

        TextView txtNome = (TextView) findViewById(R.id.txtNome);
        String nome = txtNome.getText().toString();

        TextView txtValor = (TextView) findViewById(R.id.txtValor);
        String valor = txtValor.getText().toString();

        if ("".equals(nome)){
            Toast.makeText(this, "O nome deve ser preenchido!", Toast.LENGTH_LONG).show();
        } if ("".equals(valor) || Double.parseDouble(valor) < 0){
            Toast.makeText(this, "O valor não pode ser negativo!", Toast.LENGTH_LONG).show();
        } else {
            db.execSQL("INSERT INTO produtos (nome, valor) VALUES (?,?)", new String[]{nome, valor});
            Intent in = new Intent(this, ListagemActivity.class);
            startActivity(in);
        }
    }

    public void limpar(View view) {

        TextView txtNome = (TextView) findViewById(R.id.txtNome);
        txtNome.setText("");

        TextView txtValor = (TextView) findViewById(R.id.txtValor);
        txtValor.setText("");
    }

    public void listar(View view) {
        Intent in = new Intent(this, ListagemActivity.class);
        startActivity(in);
    }
}